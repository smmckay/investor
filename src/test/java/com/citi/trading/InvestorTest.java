package com.citi.trading;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Consumer;

import javax.jms.Message;
import javax.jms.MessageListener;

import org.junit.Test;

import com.citi.trading.Trade.Result;

import static org.junit.Assert.assertThat;
import static org.hamcrest.Matchers.*;
import org.hamcrest.Matchers;

public class InvestorTest {
	public static class MockToMarket implements OrderPlacer {
		
		public int nextID = 1;
		public Map<Integer,Consumer<Trade>> callbacks = new HashMap<>();
		public Map<Integer,Trade> orders = new HashMap<>();
		
		
		@Override
		public void placeOrder(Trade order, Consumer<Trade> callback) {
			int orderID = nextID++;
			callbacks.put(orderID, callback);
			orders.put(orderID, order);	

		}
		
		
		public void processOrder(int orderID) {
			Consumer<Trade> callback = callbacks.get(orderID);
			Trade order = orders.get(orderID);
			if (callback != null && order != null) {
				callback.accept(order);
				callbacks.remove(orderID);
				orders.remove(orderID);
			} else {
				
			}
			return;
		}
		
		public void partialFillOfOrder(int orderID) {
			Consumer<Trade> callback = callbacks.get(orderID);
			Trade order = orders.get(orderID);
			order.setSize(order.getSize() / 2);
			if (callback != null && order != null) {
				callback.accept(order);
				callbacks.remove(orderID);
				orders.remove(orderID);
			} else {
				
			}
			return;
		}
		
		
		public void cancelOrder(int orderID) {
			Consumer<Trade> callback = callbacks.get(orderID);
			Trade order = orders.get(orderID);
			order.setResult(Result.REJECTED);
			if (callback != null && order != null) {
				callbacks.remove(orderID);
				orders.remove(orderID);
			} else {
				
			}
			return;
		}
	}
		/**
		 * Test that Investors can place buy orders and orders are stored in the market
		 * by creating an investor with suitable cash on hand and placing 
		 * a stock order
		 */
		@Test 
		public void testPlaceBuyOrder() {
			MockToMarket mockMarket = new MockToMarket();
			Investor mock = new Investor(50000.0, mockMarket);
			mock.buy("MRK", 100, 50);
			Trade mockTrade = new Trade("MRK", true, 100, 50);
			int orderID = 1;
			
			//Test that our market received the buyers order
			assertThat(mockMarket.callbacks, Matchers.hasKey(orderID));
			assertThat(mockMarket.orders, Matchers.hasKey(orderID));
			assertThat(mockMarket.orders.get(orderID), Matchers.samePropertyValuesAs(mockTrade));
			mockMarket.processOrder(orderID);
			
			//Test that the market found and removed the order
			assertThat(mockMarket.orders, Matchers.not(Matchers.hasKey(orderID)));
			assertThat(mockMarket.orders, Matchers.not(Matchers.hasKey(orderID)));
			
			//Test that the order was processed and cash was removed from the mock investors account
			assertThat(mock.getCash(), Matchers.closeTo(45000.0, 0.0001));
			assertThat(mock.getPortfolio(), Matchers.hasKey("MRK"));
			assertThat(mock.getPortfolio().get("MRK"), Matchers.equalTo(100));
		}
		
		/**
		 * Test that Investors can place sell orders and orders are stored in the market
		 * by creating an investor with a portfolio of stocks and executing
		 * a sell order
		 */
		@Test 
		public void testPlaceSellOrder() {
			MockToMarket mockMarket = new MockToMarket();
			Map<String, Integer> mockPortfolio = new HashMap<String, Integer>();
			mockPortfolio.put("MRK", 5000);
			
			//Start an investor 
			Investor mock = new Investor(mockPortfolio, 0, mockMarket);
			
			//Test that the shares were added to the investors portfolio
			assertThat(mock.getPortfolio(), Matchers.hasKey("MRK"));
			assertThat(mock.getPortfolio().get("MRK"), Matchers.equalTo(5000));
			
			mock.sell("MRK", 3000, 100);
			Trade mockTrade = new Trade("MRK", false, 3000, 100);
			int orderID = 1;
			
			//Test that our market received the sellers order
			assertThat(mockMarket.callbacks, Matchers.hasKey(orderID));
			assertThat(mockMarket.orders, Matchers.hasKey(orderID));
			assertThat(mockMarket.orders.get(orderID), Matchers.samePropertyValuesAs(mockTrade));
			mockMarket.processOrder(orderID);
			
			//Test that the market found and removed the order
			assertThat(mockMarket.orders, Matchers.not(Matchers.hasKey(orderID)));
			assertThat(mockMarket.orders, Matchers.not(Matchers.hasKey(orderID)));
			
			//Test that the order was processed and cash was added to the mock investors account
			assertThat(mock.getCash(), Matchers.closeTo(300000.0, 0.0001));
			assertThat(mock.getPortfolio(), Matchers.hasKey("MRK"));
			assertThat(mock.getPortfolio().get("MRK"), Matchers.equalTo(2000));
		}
		
		@Test (expected=IllegalArgumentException.class)
		public void testPlaceInvalidOrder() {
			
			//Test buying with no cash on hand
			MockToMarket mockMarket = new MockToMarket();
			Investor mock = new Investor(0, mockMarket);
			mock.buy("MRK", 1000, 500);
			
		}
		
		@Test (expected=IllegalArgumentException.class)
		public void testPlaceInvalidOrder2() {
			
			//Test placing buy order for negative stocks
			MockToMarket mockMarket = new MockToMarket();
			Investor mock = new Investor(0, mockMarket);
			mock.buy("MRK", -1000, 500);
			
		}
		
		@Test (expected=IllegalArgumentException.class)
		public void testPlaceInvalidOrder3() {
			
			//Test placing sell order for stocks that you don't own
			MockToMarket mockMarket = new MockToMarket();
			Investor mock = new Investor(0, mockMarket);
			mock.sell("MRK", 1000, 500);
			
		}
		
		@Test (expected=IllegalArgumentException.class)
		public void testPlaceInvalidOrder4() {
			MockToMarket mockMarket = new MockToMarket();
			Map<String, Integer> mockPortfolio = new HashMap<String, Integer>();
			mockPortfolio.put("MRK", 50);
			Investor mock = new Investor(mockPortfolio, 0, mockMarket);
			//Test placing order for too many stocks
			mock.sell("MRK", 1000, 500);
			
		}
		
		
		@Test
		public void testPlaceCancelledOrder() {
			MockToMarket mockMarket = new MockToMarket();
			Map<String, Integer> mockPortfolio = new HashMap<String, Integer>();
			Investor mock = new Investor(mockPortfolio, 50000, mockMarket);
			
			Trade mockTrade = new Trade("MRK", true, 1000, 50);
			int orderID = 1;
			
			mock.buy("MRK", 1000, 50);
			mockMarket.cancelOrder(orderID);
			
			//Test that the market found and removed the order
			assertThat(mockMarket.orders, Matchers.not(Matchers.hasKey(orderID)));
			assertThat(mockMarket.orders, Matchers.not(Matchers.hasKey(orderID)));
			
			//Test that the order was not processed and no cash was removed from the mock investors account
			assertThat(mock.getCash(), Matchers.closeTo(50000.0, 0.0001));
			assertThat(mock.getPortfolio(), not(hasKey("MRK")));
			
		}
		
		@Test
		public void testPlacePartialOrder() {
			MockToMarket mockMarket = new MockToMarket();
			Map<String, Integer> mockPortfolio = new HashMap<String, Integer>();
			Investor mock = new Investor(mockPortfolio, 50000, mockMarket);
			
			Trade mockTrade = new Trade("MRK", true, 1000, 50);
			int orderID = 1;
			
			mock.buy("MRK", 1000, 50);
			mockMarket.partialFillOfOrder(orderID);
			
			//Test that the market found and removed the order
			assertThat(mockMarket.orders, Matchers.not(Matchers.hasKey(orderID)));
			assertThat(mockMarket.orders, Matchers.not(Matchers.hasKey(orderID)));
			
			//Test that the order was partially processed and cash was removed from the mock investors account
			assertThat(mock.getCash(), Matchers.closeTo(25000.0, 0.0001));
			assertThat(mock.getPortfolio(), Matchers.hasKey("MRK"));
			assertThat(mock.getPortfolio().get("MRK"), Matchers.equalTo(500));
			
		}
		
		
		

		
}

